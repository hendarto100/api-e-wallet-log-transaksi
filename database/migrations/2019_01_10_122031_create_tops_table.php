<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTopsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tops', function(Blueprint $table)
		{
			$table->integer('id_top')->primary();
			$table->integer('id_admin')->unsigned()->index('id_admin');
			$table->integer('id_user')->unsigned()->index('id_user');
			$table->integer('jumlah');
			$table->string('jenis');
			$table->dateTime('waktu');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tops');
	}

}
