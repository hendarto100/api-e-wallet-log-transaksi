<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogtransferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logtransfer', function(Blueprint $table)
		{
			$table->integer('id_transfer')->primary();
			$table->integer('id_sumber')->unsigned()->index('id_sumber');
			$table->integer('id_tujuan')->unsigned()->index('id_tujuan');
			$table->integer('jumlah')->nullable();
			$table->dateTime('waktu');
			$table->integer('qrLog')->nullable();
			$table->string('jenis')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logtransfer');
	}

}
