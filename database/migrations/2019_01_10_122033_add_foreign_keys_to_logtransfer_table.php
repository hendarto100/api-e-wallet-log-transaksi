<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLogtransferTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('logtransfer', function(Blueprint $table)
		{
			$table->foreign('id_sumber', 'logtransfer_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_tujuan', 'logtransfer_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('logtransfer', function(Blueprint $table)
		{
			$table->dropForeign('logtransfer_ibfk_1');
			$table->dropForeign('logtransfer_ibfk_2');
		});
	}

}
