<?php

namespace App\Http\Controllers;

use App\User;
use App\LogTransfer;
use Illuminate\Http\Request;
use Auth;

class LogTransferController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request) 
    {   
        $logTransfer = Auth::user()->logTransferTo()->get(); //sisi yang mengirim
    
        return response()->json(['status' => 'success','result' => $logTransfer]);
    }

    public function from(Request $request) //sisi yang menerima
    {   
        
        $logTransfer = Auth::user()->logTransferFrom()->get();
        
        return response()->json(['status' => 'success','result' => $logTransfer]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    public function store(Request $request)
    {
        $TransactionSuccess = NULL; //cek transaksi

        //dd($request->jumlah);
        $this->validate($request, [
            //'id_transfer' => 'required', karna sudah Auto Increment
            'id_tujuan' => 'required',
            'jumlah' => 'required',
            'jenis' => 'required'
         ]);

         $requestData = $request->all(); //ambil semua data user yang aktif
         $requestData['id_sumber'] = Auth::user()->id; //ubah data request didalam input menjadi user id
         $requestData['waktu'] = date("Y-m-d H:i:s"); //ubah data waktu di request menjadi time skrg

        if(Auth::user()->logTransferTo()->Create($requestData)){ //Post Transaction
            $TransactionSuccess = TRUE; //berhasil
             
            if($TransactionSuccess == TRUE){   //menambah Saldo ke User bila transaksi berhasil
                $saldoNow = Auth::user()->saldo + $request->jumlah;
                $User = Auth::user();
                $User->saldo = $saldoNow;
                $User->save();

                return response()->json(['status' => 'success']);
            }
            
        }else{
            return response()->json(['status' => 'fail']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $todo = Todo::where('id', $id)->get();
        return response()->json($todo);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todo::where('id', $id)->get();
        return view('todo.edittodo',['todos' => $todo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'todo' => 'filled',
        'description' => 'filled',
        'category' => 'filled'
         ]);
        $todo = Todo::find($id);
        if($todo->fill($request->all())->save()){
           return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'failed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Todo::destroy($id)){
             return response()->json(['status' => 'success']);
        }
    }
}