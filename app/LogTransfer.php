<?php
 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
use Illuminate\Contracts\Auth\Authenticatable;
 
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
 
 
class LogTransfer extends Model
{

protected $table = 'logtransfer';
public $timestamps = false;
protected $fillable = ['id_transfer','id_sumber','id_tujuan','jumlah','waktu','qrLog','jenis'];
protected $appends = ['sumber', 'tujuan']; //dyn. appends keys to json
protected $hidden = [
    'id_sumber','id_tujuan','userSumber','userTujuan'
    ];

public function userSumber()
{
    return $this->belongsTo('App\User','id_sumber');
}

public function userTujuan()
{
    return $this->belongsTo('App\User','id_tujuan');
}



public function getSumberAttribute($value)       // gets the username for id_source
{
    return $this->userSumber->name;
}

public function getTujuanAttribute($value)  // gets the username for id_destination
{
    return $this->userTujuan->name;
}

}
 