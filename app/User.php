<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Model //implements Authenticatable
{
    //
    use AuthenticableTrait;
    public $timestamps = false;
    protected $fillable = ['username','email','password','userimage','saldo'];
    protected $table='users';
    protected $hidden = [
    'password'
    ];


    /*
    * Get Todo of User
    *
    */
    public function todo()
    {
        return $this->hasMany('App\Todo','user_id');
    }
    public function logtransfer()
    {
        return $this->hasMany('App\LogTransfer','id_sumber');
    }
    public function logTransferTo()
    {
        return $this->hasMany('App\LogTransfer','id_sumber');
    }

    public function logTransferFrom()
    {
        return $this->hasMany('App\LogTransfer','id_tujuan');
    }
}
