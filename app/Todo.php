<?php
 
namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
use Illuminate\Contracts\Auth\Authenticatable;
 
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
 
 
class Todo extends Model
{

protected $table = 'todo';

protected $fillable = ['todo','category','user_id','description'];

}
 